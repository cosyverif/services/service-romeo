package org.cosyverif.service.romeo;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.cosyverif.Configuration;
import org.cosyverif.alligator.XML;
import org.cosyverif.alligator.service.util.FileUtility;
import org.cosyverif.alligator.util.Utility;
import org.cosyverif.model.Model;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.junit.Assert;

public final class GrmlParserTest {
  private File directory;
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_RESET = "\u001B[0m";

  /** Create temp folder */
  @Before
  public void setUp() {
    directory = Utility.createTemporaryDirectory();
    Configuration.instance()
        .temporaryDirectory(directory);
  }

  /** Delete temp folder */
  @After
  public void tearDown() {
    Utility.deleteDirectory(directory);
    Configuration.instance()
        .temporaryDirectory(new File(System.getProperty("java.io.tmpdir")));
  }

  /**
   * Prints a message with green color
   *
   * @param msg string to be printed
   */
  public void printSuccessMsg(String msg) {
    System.out.println(ANSI_GREEN + msg + ANSI_RESET);
  }

  public void compareEqual(File expected, File actual) throws IOException {
    // Read the contents of the output file into a string
    String outputString = Files.readString(actual.toPath()).replaceAll("\\s+", " ");
    String expectedOutput = Files.readString(expected.toPath()).replaceAll("\\s+", " ");

    assertEquals(expectedOutput, outputString);
  }

  /**
   * Loads a model from a GrML file in the classpath.
   *
   * @param path the path to the GrML file.
   * @return the model
   */
  public final Model loadModelResource(String path) {
    try {
      if (path.startsWith("/")) {
        path = path.substring(1);
      }

      File file = File.createTempFile("model_", ".grml", Configuration.instance().temporaryDirectory());
      FileUtility.copyFile(getClass().getClassLoader().getResource(path).openConnection().getURL(), file);
      Model result = (Model) XML.fromFile(file);
      file.delete();
      return result;
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Loads a file in the classpath.
   *
   * @param path the path to the file.
   * @return the file
   */
  public final File loadFileResource(String path) {
    try {
      if (path.startsWith("/")) {
        path = path.substring(1);
      }
      File file = File.createTempFile("file_", ".tmp", Configuration.instance()
          .temporaryDirectory());
      FileUtility.copyFile(getClass().getClassLoader()
          .getResource(path)
          .openConnection()
          .getURL(), file);
      return file;
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Test
  public void testConstructor() {
    try {
      // Create a model object to pass to the GrmlParser constructor
      Model model = new Model();
      // Create a GrmlParser object using the constructor
      GrmlParser parser = new GrmlParser(model);
      // Check that the model field of the GrmlParser object is set correctly
      assertEquals(model, parser.model());
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail("Unable to instantiate the GrmlParser class.");
    }
  }

  @Test
  public void testExampleModelParseCTS() throws DOMException, Exception {
    // Create parser
    GrmlParser parser = new GrmlParser(loadModelResource("/models/example.grml"));
    // Call the parse method
    File model = parser.parseToCTS();
    File expected = loadFileResource(("/models/example.cts"));
    compareEqual(expected, model);
    printSuccessMsg("[SUCCESS] testExampleModelParseCTS");
  }

  @Test
  public void testExampleModelParseXML() throws DOMException, Exception {
    // Create parser
    GrmlParser parser = new GrmlParser(loadModelResource("/models/example.grml"));
    // Call the parse method
    File model = parser.parseToXML();
    File expected = loadFileResource(("/models/example.xml"));
    compareEqual(expected, model);
    printSuccessMsg("[SUCCESS] testExampleModelParseXML");
  }
}
