package org.cosyverif.service.romeo;

import java.io.File;

import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.model.Model;
import org.w3c.dom.DOMException;

@Service(name = "Export into Romeo format", 
    help = "Export of this Petri net in the Romeo format", 
    version = "1.0", 
    tool = "Romeo", 
    index = 2,
    authors = {
        "Jaime Arias",
    }, 
    keywords = {})
public class Translation extends BinaryService {
  GrmlParser parser;

  private enum Format {
    CTS, XML
  };

  // Model file
  @Parameter(name = "Model", help = "Petri net model", direction = Direction.IN, formalism = "http://formalisms.cosyverif.org/parametric-pt-net.fml")
  private Model model;

  // output format
  @Parameter(name = "Format", help = "Output format", direction = Direction.IN, underlying = Format.class)
  private Format format;

  // output
  @Parameter(name = "Romeo file", help = "File with the output", direction = Direction.OUT, contenttype = "plain/text")
  private File RomeoFile = null;

  @Launch
  public Task executeBinary() throws DOMException, Exception {
    // parse GrML model
    parser = GrmlParser.create(model);
    final File model = format.equals(Format.XML) ? parser.parseToXML() : parser.parseToCTS();

    RomeoFile = model.getAbsoluteFile();
    return null;
  }
}
