package org.cosyverif.service.romeo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringEscapeUtils;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.LineOutputHandler;
import org.cosyverif.model.Model;
import org.w3c.dom.DOMException;

@Service(name = "CheckPTCTL",
    help = "Parameter synthesis for the model-checking of a subset of the TCTL timed logic",
    version = "3.9.1",
    tool = "Romeo",
    index = 1,
    authors = {
      "Olivier H. Roux",
      "Didier Lime",
      "Louis-Marie Traonouez",
      "Charlotte Seidner"
    },
    keywords = {})
public final class CheckPTCTL extends BinaryService {
  GrmlParser parser;

  // Model file
  @Parameter(name = "Model", help = "Petri net model", direction = Direction.IN, formalism = "http://formalisms.cosyverif.org/parametric-pt-net.fml")
  private Model model;

  // PTCTL property
  @Parameter(name = "Property", help = "PTCTL formula to check", direction = Direction.IN)
  private String property;

  // Option: -nored
  @Parameter(name = "Minimize unions of polyhedra", help = "Try to minimize unions of polyhedra", direction = Direction.IN)
  public boolean minimizeUnions = true;

  // Option: -restrict
  @Parameter(name = "Restrict exploration", help = "Try to restrict exploration to parameter valuations not found already", direction = Direction.IN)
  public boolean restrict = true;

  // Option: -timed_trace
  @Parameter(name = "Compute timed trace", help = "Compute timed trace", direction = Direction.IN)
  public boolean timedTrace = false;

  // Option: -absolute
  @Parameter(name = "Compute absolute time trace", help = "Compute absolute time trace", direction = Direction.IN)
  public boolean absoluteTimedTrace = false;

  // output
  @Parameter(name = "Result", help = "Result from checking formula", direction = Direction.OUT, multiline = true)
  public String result = "";

  @Launch
  public Task executeBinary() throws DOMException, Exception {
    // parse GrML model
    parser = GrmlParser.create(model);
    final File model = parser.parseToCTS();

    // update input file with the property
    this.appendPropertyToFile(model.getAbsolutePath(), property, minimizeUnions, restrict, timedTrace,
        absoluteTimedTrace);

    // set romeo command-line
    final CommandLine command = new CommandLine("romeo-3.9.1/bin/romeo-cli");
    command.addArgument("-m");
    command.addArgument(model.getAbsolutePath());

    // run command
    return task(command)
        .workingDirectory(baseDirectory())
        .out(new LineOutputHandler(this) {
          @Override
          public void call(String line) {
            result += line + "\n";
          }
        });
  }

  /**
   * Append the property to the model
   *
   * @param filename
   * @param property
   * @throws IOException
   */
  private void appendPropertyToFile(String filename, String property, boolean minimizeUnions, boolean restrict,
      boolean timedTrace,
      boolean absoluteTimedTrace) throws IOException {
    BufferedWriter out = new BufferedWriter(new FileWriter(filename, true));

    // parse options
    ArrayList<String> options = new ArrayList<String>();
    if (!minimizeUnions) {
      options.add("nored");
    }
    if (restrict) {
      options.add("restrict");
    }
    if (timedTrace) {
      options.add("timed_trace");
      if (absoluteTimedTrace) {
        options.add("absolute");
      }
    }

    String sanitizeProperty = StringEscapeUtils.unescapeXml(property);
    out.write("\ncheck [ " + String.join(", ", options) + " ] " + sanitizeProperty);
    out.close();
  }

  @Example(name = "Example of EF checking", help = "Example of EF checking")
  public CheckPTCTL example() {
    CheckPTCTL service = new CheckPTCTL();
    service.model = loadModelResource("/models/example.grml");
    service.timedTrace = true;
    service.property = "AG ((itemready<=1 and buffer<=1 and itemreceived<=1 and readyconsumer<=1 and readyproducer<=1))";
    return service;
  }
}
