package org.cosyverif.service.romeo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;

import org.cosyverif.Configuration;
import org.cosyverif.model.Model;
import org.cosyverif.model.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.cosyverif.model.Arc;
import org.cosyverif.model.Attribute;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.NONE)
@Accessors(chain = true, fluent = true)
public class GrmlParser {
  /** GRML Model */
  private Model model;

  /**
   * Constructor
   *
   * @param model Grml Model
   */
  public GrmlParser(Model model) {
    this.model = model;
  }

  /**
   * Creates a parser from Grml to Romeo
   *
   * @param model Grml Model
   * @return GrmlParser object
   */
  public static GrmlParser create(Model model) {
    return new GrmlParser(model);
  }

  /**
   * Method to parse to XML version of Romeo
   *
   * @return File
   * @throws DOMException
   * @throws Exception
   */
  public File parseToXML() throws DOMException, Exception {
    return parse(new XMLParser());
  }

  /**
   * Method to parse to CTS version of Romeo
   *
   * @return File
   * @throws DOMException
   * @throws Exception
   */
  public File parseToCTS() throws DOMException, Exception {
    return parse(new TextParser());
  }

  /**
   * Generates a file handled by the romeo tool
   *
   * @param parser Parser to Romeo file
   * @return File object of the generated file
   * @throws Exception
   * @throws DOMException
   */
  public File parse(RomeoParser parser) throws DOMException, Exception {
    /** Create .net input file */
    File outputDirectory = Configuration.instance().temporaryDirectory();
    File fileOutput = File.createTempFile("pitpn_", "", outputDirectory);
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput));

    /** Parse GrML into a Petri net model */
    Net net = parseNet();

    /** write file */
    writer.write(parser.parse(net));
    writer.close();

    return fileOutput;
  }

  private Net parseNet() {

    /**
     * Parsing model title
     */
    String title = "";
    String initialConstraint = "";
    for (Attribute attr : model.getAttribute()) {
      if (attr.getName().equals("name") && attr.getContent().size() == 1) {
        title = (String) attr.getContent().get(0);
      }
      if (attr.getName().equals("initialConstraint")) {
        Attribute constraintAttr = this.findAttributeByName(attr.getContent(), "constraintExpr").get(0);
        if (constraintAttr.getContent().size() == 1) {
          initialConstraint = this.getTextAttribute(constraintAttr);
        }
      }
    }

    // create a new Petri net
    Net net = new Net(title);
    net.initialConstraint(initialConstraint);

    // parse nodes
    for (Node node : model.getNode()) {
      String id = node.getId().toString();
      String label = "";
      int marking = 0; // default marking value
      String lowerBound = "0"; // default lower bound
      String higherBound = "inf"; // default higher bound
      String x = node.getX().toString();
      String y = node.getY().toString();

      // Parse attributes
      for (Object content : node.getNodeContent()) {
        Attribute attr = ((Attribute) content);
        List<Object> attributeContent = attr.getContent();

        String attributeName = attr.getName();
        if (attributeName.equals("name")) {
          // parse name
          label = attributeContent.get(0).toString();
        } else if (attributeName.equals("marking")) {
          // parse marking
          marking = Integer.valueOf(attributeContent.get(0).toString());
        } else if (attributeName.equals(("lowerBound"))) {
          // parse lower bound
          lowerBound = this.parseBound(attributeContent, lowerBound);
        } else if (attributeName.equals(("higherBound"))) {
          // parse higher bound
          higherBound = this.parseBound(attributeContent, higherBound);
        }
      }

      // node is a place
      if (node.getNodeType().equals("place")) {
        net.addPlace(new Place(id, label, marking, x, y));
      } else if (node.getNodeType().equals("transition")) {
        net.addTransition(new Transition(id, label, lowerBound, higherBound, x, y));
      }
    }

    // parse arcs
    for (Arc arc : model.getArc()) {
      String arcType = arc.getArcType();
      String source = arc.getSource().toString();
      String target = arc.getTarget().toString();

      Attribute valuationAttr = this.findAttributeByName(arc.getArcContent(), "valuation").get(0);
      int valuation = Integer.parseInt(this.getTextAttribute(valuationAttr));

      if (arcType.equals("arc")) {
        if (net.isTransition(target)) {
          // incoming arc
          net.getTransition(target).addIncomingArc(source, valuation);
        } else if (net.isTransition(source)) {
          // outgoing arc
          net.getTransition(source).addOutgoingArc(target, valuation);
        }
      } else if (arcType.equals("inhibitorarc")) {
        net.getTransition(target).addIncommingInhibitorArc(source, valuation);
      }
    }

    return net;
  }

  /**
   * This method is used to extract the value of an attribute with the given name
   * from a list of objects.
   *
   * @param attributeList the list of attributes to search for the attribute
   * @param name          the name of the attribute whose value is to be extracted
   * @return the value of the attribute with the given name, otherwise null
   */
  private <E> List<Attribute> findAttributeByName(List<E> attributeList, String name) {
    List<Attribute> found = new ArrayList<Attribute>();
    for (E object : attributeList) {
      if (object instanceof Attribute) {
        Attribute attr = (Attribute) object;
        if (attr.getName().equals(name)) {
          found.add(attr);
        }
      }
    }
    return found;
  }

  /**
   * This method gets the content of an Attribute and trim it
   * For instance, with <attribute name="name">d1</attribute> as input, the
   * method will return d1.
   */
  private String getTextAttribute(Attribute attribute) {
    return ((String) attribute.getContent().get(0)).trim();
  }

  /**
   * Parse the bounds of the transitions
   */
  private String parseBound(List<Object> attrContent, String defaultValue) {
    List<Object> boundAttr = this.findAttributeByName(attrContent, "bound").get(0).getContent();

    // check for a numeric bound
    List<Attribute> valueAttr = this.findAttributeByName(boundAttr, "numValue");
    if (valueAttr.size() == 1) {
      return this.getTextAttribute(valueAttr.get(0));
    }

    // check for a parametric bound
    List<Attribute> paramAttr = this.findAttributeByName(boundAttr, "name");
    if (paramAttr.size() == 1) {
      return this.getTextAttribute(paramAttr.get(0));
    }

    return defaultValue;
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Place {
  private String x;
  private String y;
  private String id;
  private String label;
  private int marking;

  public Place(String id, String label, int marking, String x, String y) {
    this.id = id;
    this.label = label;
    this.marking = marking;
    this.x = x;
    this.y = y;
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Arrow {
  private String source;
  private String target;
  private int valuation;
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Transition {
  private String x;
  private String y;
  private String id;
  private String label;
  private String lowerBound;
  private String higherBound;
  private ArrayList<Arrow> outgoingArcs;
  private ArrayList<Arrow> incomingArcs;
  private ArrayList<Arrow> incomingInhibitorArcs;

  public Transition(String id, String label, String lowerBound, String higherBound, String x, String y) {
    this.x = x;
    this.y = y;
    this.id = id;
    this.label = label;
    this.lowerBound = lowerBound;
    this.higherBound = higherBound;
    this.outgoingArcs = new ArrayList<Arrow>();
    this.incomingArcs = new ArrayList<Arrow>();
    this.incomingInhibitorArcs = new ArrayList<Arrow>();
  }

  public void addOutgoingArc(String targetId, int valuation) {
    this.outgoingArcs.add(new Arrow(id, targetId, valuation));
  }

  public void addIncomingArc(String sourceId, int valuation) {
    this.incomingArcs.add(new Arrow(sourceId, id, valuation));
  }

  public void addIncommingInhibitorArc(String sourceId, int valuation) {
    this.incomingInhibitorArcs.add(new Arrow(sourceId, id, valuation));
  }

}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Net {
  private String title;
  private String initialConstraint;
  private HashMap<String, Place> places = new HashMap<String, Place>();
  private HashMap<String, Transition> transitions = new HashMap<String, Transition>();

  public Net(String title) {
    this.title = title;
  }

  public void addTransition(Transition t) {
    this.transitions.put(t.id(), t);
  }

  public void addPlace(Place p) {
    this.places.put(p.id(), p);
  }

  public Transition getTransition(String id) {
    return this.transitions.get(id);
  }

  public Place getPlace(String id) {
    return this.places.get(id);
  }

  public ArrayList<Transition> transitions() {
    return new ArrayList<Transition>(this.transitions.values());
  }

  public ArrayList<Place> places() {
    return new ArrayList<Place>(this.places.values());
  }

  public Boolean isTransition(String id) {
    return this.transitions.containsKey(id);
  }

  public Boolean isPlace(String id) {
    return this.places.containsKey(id);
  }

}

interface RomeoParser {
  public String parse(Net model);
}

class TextParser implements RomeoParser {
  public String parse(Net model) {
    StringBuilder sb = new StringBuilder();

    /** parse initial constraints */
    if (model.initialConstraint() != "") {
      sb.append("parameters ").append(model.initialConstraint()).append("\n");
    }

    /** parse places */
    sb.append("initially { int ");
    ArrayList<String> placesLabel = new ArrayList<String>();
    for (Place p : model.places()) {
      placesLabel.add(parse(p));
    }
    sb.append(String.join(", ", placesLabel));
    sb.append("; }").append("\n");

    /** parse transitions */
    for (Transition t : model.transitions()) {
      sb.append(parse(t, model)).append("\n");
    }

    sb.append("// insert TCTL formula here : check formula");
    return sb.toString();
  }

  private String parse(Place p) {
    StringBuilder sb = new StringBuilder();
    sb.append(p.label()).append("=").append(p.marking());
    return sb.toString();
  }

  private String parse(Transition t, Net m) {
    StringBuilder sb = new StringBuilder();
    sb.append("transition ");

    // iterate over the pre, post arcs
    ArrayList<String> guards = new ArrayList<String>();
    ArrayList<String> updates = new ArrayList<String>();
    for (Arrow arc : t.incomingArcs()) {
      String source = m.getPlace(arc.source()).label();
      guards.add(source + " >= " + arc.valuation());
      updates.add(source + " = " + source + " - " + arc.valuation());
    }
    sb.append("[ intermediate { ").append(String.join(", ", updates)).append("; }, speed=1*(1) ] ");

    // add transition information
    sb.append(t.label()).append(" [").append(t.lowerBound()).append(",").append(t.higherBound()).append("]\n");

    // iterate over inhibitor arcs
    for (Arrow arc : t.incomingInhibitorArcs()) {
      String source = m.getPlace(arc.source()).label();
      guards.add(source + " < " + arc.valuation());
      updates.add(source + " = " + source);
    }
    sb.append("\twhen (").append(String.join(" and ", guards)).append(")\n");

    // iterate over outgoing arcs
    for (Arrow arc : t.outgoingArcs()) {
      String target = m.getPlace(arc.target()).label();
      updates.add(target + " = " + target + " + " + arc.valuation());
    }
    sb.append("\t{ ").append(String.join(", ", updates)).append("; }");

    return sb.toString();
  }
}

class XMLParser implements RomeoParser {
  public String parse(Net model) {
    try {
      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      Document doc = docFactory.newDocumentBuilder().newDocument();

      // parse model
      parse(doc, model);

      // transform to string
      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer transformer = tf.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
      StringWriter writer = new StringWriter();
      transformer.transform(new DOMSource(doc), new StreamResult(writer));

      return writer.getBuffer().toString();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }

  private Element parse(Document doc, Net model) throws Exception {
    // root element
    Element rootElement = doc.createElement("TPN");
    rootElement.setAttribute("name", model.title() + ".xml");
    doc.appendChild(rootElement);

    // parse places
    for (Place p : model.places()) {
      rootElement.appendChild(parse(doc, p));
    }

    // parse transitions
    for (Transition t : model.transitions()) {
      rootElement.appendChild(parse(doc, t));
    }

    // parse arcs
    for (Transition t : model.transitions()) {
      for (Arrow a : t.incomingArcs()) {
        rootElement.appendChild(parse(doc, a, "PlaceTransition"));
      }
      for (Arrow a : t.outgoingArcs()) {
        rootElement.appendChild(parse(doc, a, "TransitionPlace"));
      }
      for (Arrow a : t.incomingInhibitorArcs()) {
        rootElement.appendChild(parse(doc, a, "logicalInhibitor"));
      }
    }

    // parse initial constraint
    Element misc = doc.createElement("misc");
    Pattern pattern = Pattern.compile("(\\w+)\\s*([<>=]=?)\\s*(\\w+)");
    List<String> constraints = Arrays.asList(model.initialConstraint().split(" and "));
    for (String c : constraints) {
      Element constraint = doc.createElement("constraint");
      Matcher matcher = pattern.matcher(c);
      if (matcher.find()) {
        constraint.setAttribute("left", matcher.group(1) + " ");
        constraint.setAttribute("right", " " + matcher.group(3));
        constraint.setAttribute("op", parseOp(matcher.group(2)));
        misc.appendChild(constraint);
      }

    }
    rootElement.appendChild(misc);

    // parse timedCost
    rootElement.appendChild(doc.createElement("timedCost"));

    // coloured tokens
    Element tokenColor = doc.createElement("nbTokenColor");
    tokenColor.appendChild(doc.createTextNode("0"));
    rootElement.appendChild(tokenColor);

    // declarations
    rootElement.appendChild(doc.createElement("declaration"));

    // project
    Element project = doc.createElement("project");
    project.setAttribute("nbinput", "0");
    project.setAttribute("openinput", "0");
    project.setAttribute("nbinclude", "0");
    rootElement.appendChild(project);

    // preferences
    Element preferences = doc.createElement("preferences");
    Element colorPlace = doc.createElement("colorPlace");
    colorPlace.setAttribute("c0", "SkyBlue2");
    colorPlace.setAttribute("c1", "#ffbebe");
    colorPlace.setAttribute("c2", "cyan");
    colorPlace.setAttribute("c3", "green");
    colorPlace.setAttribute("c4", "yellow");
    colorPlace.setAttribute("c5", "brown");
    preferences.appendChild(colorPlace);
    Element colorTransition = doc.createElement("colorTransition");
    colorTransition.setAttribute("c0", "yellow");
    colorTransition.setAttribute("c1", "gray");
    colorTransition.setAttribute("c2", "cyan");
    colorTransition.setAttribute("c3", "green");
    colorTransition.setAttribute("c4", "SkyBlue2");
    colorTransition.setAttribute("c5", "brown");
    preferences.appendChild(colorTransition);
    Element colorArc = doc.createElement("colorArc");
    colorArc.setAttribute("c0", "black");
    colorArc.setAttribute("c1", "gray");
    colorArc.setAttribute("c2", "blue");
    colorArc.setAttribute("c3", "#beb760");
    colorArc.setAttribute("c4", "#be5c7e");
    colorArc.setAttribute("c5", "#46be90");
    preferences.appendChild(colorArc);
    rootElement.appendChild(preferences);

    return rootElement;
  }

  private Element parse(Document doc, Place p) {
    Element place = doc.createElement("place");
    place.setAttribute("id", p.id());
    place.setAttribute("identifier", p.label());
    place.setAttribute("label", p.label());
    place.setAttribute("initialMarking", Integer.toString(p.marking()));

    // TODO: ask what is this
    place.setAttribute("eft", "0");
    place.setAttribute("lft", "inf");

    // graphics node
    Element graphics = doc.createElement("graphics");
    graphics.setAttribute("color", "0");

    // position node
    Element position = doc.createElement("position");
    position.setAttribute("x", p.x());
    position.setAttribute("y", p.y());
    graphics.appendChild(position);

    // deltaLabel node
    Element deltaLabel = doc.createElement("deltaLabel");
    deltaLabel.setAttribute("deltax", "0");
    deltaLabel.setAttribute("deltay", "0");
    graphics.appendChild(deltaLabel);

    // scheduling node
    Element scheduling = doc.createElement("scheduling");
    scheduling.setAttribute("gamma", "0");
    scheduling.setAttribute("omega", "0");

    place.appendChild(graphics);
    place.appendChild(scheduling);

    return place;
  }

  private Element parse(Document doc, Transition t) {
    Element transition = doc.createElement("transition");
    transition.setAttribute("id", t.id());
    transition.setAttribute("identifier", t.label());
    transition.setAttribute("label", t.label());

    // attributes no supported by the fml specification
    transition.setAttribute("speed", "1");
    transition.setAttribute("priority", "0");
    transition.setAttribute("cost", "0");
    transition.setAttribute("unctrl", "0");
    transition.setAttribute("obs", "1");
    transition.setAttribute("guard", "");

    // intervals (lower bound)
    transition.setAttribute("eft", t.lowerBound());
    transition.setAttribute("eft_param", t.lowerBound());

    // intervals (upper bound)
    transition.setAttribute("lft", t.higherBound());
    transition.setAttribute("lft_param", t.higherBound());

    // graphics node
    Element graphics = doc.createElement("graphics");
    graphics.setAttribute("color", "0");

    // position node
    Element position = doc.createElement("position");
    position.setAttribute("x", t.x());
    position.setAttribute("y", t.y());
    graphics.appendChild(position);

    // deltaLabel node
    Element deltaLabel = doc.createElement("deltaLabel");
    deltaLabel.setAttribute("deltax", "0");
    deltaLabel.setAttribute("deltay", "0");
    graphics.appendChild(deltaLabel);

    // deltaGuard node
    Element deltaGuard = doc.createElement("deltaGuard");
    deltaGuard.setAttribute("deltax", "20");
    deltaGuard.setAttribute("deltay", "-20");
    graphics.appendChild(deltaGuard);

    // deltaUpdate node
    Element deltaUpdate = doc.createElement("deltaUpdate");
    deltaUpdate.setAttribute("deltax", "20");
    deltaUpdate.setAttribute("deltay", "10");
    graphics.appendChild(deltaUpdate);

    // deltaSpeed node
    Element deltaSpeed = doc.createElement("deltaSpeed");
    deltaSpeed.setAttribute("deltax", "-20");
    deltaSpeed.setAttribute("deltay", "5");
    graphics.appendChild(deltaSpeed);

    // deltaCost node
    Element deltaCost = doc.createElement("deltaCost");
    deltaCost.setAttribute("deltax", "-20");
    deltaCost.setAttribute("deltay", "5");
    graphics.appendChild(deltaCost);

    // update node
    Element update = doc.createElement("update");
    update.appendChild(doc.createCDATASection(""));

    transition.appendChild(graphics);
    transition.appendChild(update);

    return transition;
  }

  private Element parse(Document doc, Arrow a, String type) {
    Element arc = doc.createElement("arc");
    arc.setAttribute("type", type);
    arc.setAttribute("weight", Integer.toString(a.valuation()));
    arc.setAttribute("tokenColor", "-1");

    if (type.equals("TransitionPlace")) {
      arc.setAttribute("place", a.target());
      arc.setAttribute("transition", a.source());
    } else {
      arc.setAttribute("place", a.source());
      arc.setAttribute("transition", a.target());
      arc.setAttribute("inhibitingCondition", "");
    }

    // nail node
    Element nail = doc.createElement("nail");
    nail.setAttribute("xnail", "0");
    nail.setAttribute("ynail", "0");
    arc.appendChild(nail);

    // graphics node
    Element graphics = doc.createElement("graphics");
    graphics.setAttribute("color", "0");
    arc.appendChild(graphics);

    return arc;
  }

  private String parseOp(String op) throws Exception {
    if (op.equals(">")) {
      return "Greater";
    } else if (op.equals(">=")) {
      return "GreaterOrEqual";
    } else if (op.equals("<")) {
      return "Lower";
    } else if (op.equals("<=")) {
      return "LowerOrEqual";
    } else if (op.equals("==")) {
      return "Equal";
    } else {
      throw new Exception(op + " is not supported by the parser");
    }
  }
}